;; NOTE: This file is generated from ..

(define-module (vm)
  #:use-module (srfi srfi-1)
  #:use-module (base-system)
  #:use-module (gnu))

(operating-system
 (inherit base-operating-system)
 (host-name "vm")

 (file-systems (cons*
                (file-system
                 (device (file-system-label "root"))
                 (mount-point "/")
                 (type "ext4"))
                (file-system
                 (device (file-system-label "EFI"))
                 (mount-point "/boot/efi")
                 (type "vfat"))
                %base-file-systems)))
